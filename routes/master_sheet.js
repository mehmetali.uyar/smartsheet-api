const express = require('express'),
  router = express.Router(),
  asyncHandler = require('express-async-handler'),
  smartsheet = require('../modules/smartsheet_url'),
    sheet = require('./../constants/sheet'),
    master_sheet = require('./../constants/master_sheet'),
  logging = require('./../modules/logging'),
    read_json = require('./../modules/read_json'),
  bodyParser = require('body-parser').json();

router.use((req, res, next) => {
    if (req.headers['smartsheet-hook-challenge']) {
        res.setHeader('smartsheet-hook-response', req.headers['smartsheet-hook-challenge']);
        res.send({
            smartsheetHookResponse: req.headers['smartsheet-hook-challenge']
        });
    } else {
        next();
    }
});
/** req body
 * {
    "nonce": "62c0fc77-095e-4831-b8a7-2bf38995583d",
    "timestamp": "2022-05-24T07:03:36.758+00:00",
    "webhookId": 2477581984393092,
    "scope": "sheet",
    "scopeObjectId": 944130760173444,
    "events": [
        {
            "objectType": "cell",
            "eventType": "created",
            "rowId": 4256072584062852,
            "columnId": 5586258751711108,
            "userId": 7738337504061316,
            "timestamp": "2022-05-24T07:03:30.000+00:00"
        }
    ]
    }
*/
router.post('/', bodyParser, asyncHandler(async(req, res) => {
    var { events } = req.body;
    var { rows }  = await smartsheet.getSheet(master_sheet.sheet_id);
    var allSheets = await smartsheet.getSheets();
    var rowIds = [...new Set(events.map(x => x.rowId))];
    for(let rowId of rowIds)
    {
        var cells = rows.filter(x => x.id === rowId).shift().cells;
        const sheetName = cells.filter(x => x.columnId === Number(master_sheet.sheet_name_column_id)).shift().value;
        const dataColumnName = cells.filter(x => x.columnId === Number(master_sheet.data_column_id)).shift().value;
        const listColumnName = cells.filter(x => x.columnId === Number(master_sheet.dropdown_column_id)).shift().value;
        const sheets = allSheets.data.filter(x => x.name == sheetName);
        if(sheets !== undefined && sheets.length !== 0) {
            const sheetId = sheets.shift().id;
            let {columns} = await smartsheet.getSheet(sheetId);
            const dataColumns = columns.filter(x => x.title === dataColumnName);
            const listColumns = columns.filter(x => x.title === listColumnName);
            if (dataColumns !== undefined && listColumns !== undefined) {
                const dataColumnId = dataColumns.shift().id;
                const listColumnId = listColumns.shift().id;
                const companyColumns = read_json.getCompanyColumns(sheetId);
                const webhookBody = {
                    "name": sheetId + "_API_Webhook",
                    "callbackUrl": "https://smartsheetwebapp.azurewebsites.net/columns/" + sheetId,
                    "scope": "sheet",
                    "scopeObjectId": sheetId, // this is the id of the sheet
                    "subscope": { // add this to filter down changes on the sheet to a specific column(s)
                        "columnIds": [
                            dataColumnId
                        ]
                    },
                    "events": ["*.*"],
                    "version": 1
                };
                if (companyColumns === undefined) {
                    const webhookResp = await smartsheet.createWebhook(webhookBody);
                    if (webhookResp.resultCode === 0) {
                        const webhookId = webhookResp.result.id;
                        const enableBody = {
                            "enabled": true
                        };
                        const webhookEnableResp = await smartsheet.updateWebhook(webhookId, enableBody);
                        logging.log("WEBHOOK Enabled : " + webhookId);
                        read_json.writeCompanyColumns(sheetId, dataColumnId, listColumnId, webhookId);
                        logging.log("WEBHOOK saved to file.");
                        res.sendStatus(200);
                    } else {
                        logging.err("WEBHOOK RESPOND ERROR ");
                        logging.json(webhookResp);
                        res.sendStatus(500);
                    }

                } else {
                    const webhookResp = await smartsheet.updateWebhook(companyColumns.webhookId, webhookBody);
                    read_json.writeCompanyColumns(sheetId, dataColumnId, listColumnId, companyColumns.webhookId);
                    res.sendStatus(200);
                }
            } else {
                res.sendStatus(500);
            }
        } else {
            res.sendStatus(500);
        }


    }
}));

router.use((error, req, res, next) => {
    if (error) {
        logging.err(error);
        res.send(error);
        res.sendStatus(500);
    } else {
        res.sendStatus(200);
    }
});

module.exports = router;
