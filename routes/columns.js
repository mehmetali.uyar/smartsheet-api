const express = require('express'),
  router = express.Router(),
  asyncHandler = require('express-async-handler'),
  // https://zellwk.com/blog/async-await-express/
  smartsheet = require('../modules/smartsheet_url'),
    sheet = require('./../constants/sheet'),
    master_sheet = require('./../constants/master_sheet'),
    read_json = require('./../modules/read_json'),
  logging = require('./../modules/logging'),
  bodyParser = require('body-parser').json();

const updateRows = async function (sheet_id, watch_column_id, update_column_id)
{
    const { rows } = await smartsheet.getSheet(sheet_id, watch_column_id);
    const names = rows.map(x => x.cells.length ? x.cells[0].value : false);
    return await smartsheet.updateColumn(sheet_id, update_column_id, names);
}

router.use((req, res, next) => {
    if (req.headers['smartsheet-hook-challenge']) {
        logging.log("SMART HOOK CHALLENGE!");
        res.setHeader('smartsheet-hook-response', req.headers['smartsheet-hook-challenge']);
        res.send({
            smartsheetHookResponse: req.headers['smartsheet-hook-challenge']
        });
    } else {
        next();
    }
});
/** response body
 * {
    "nonce": "62c0fc77-095e-4831-b8a7-2bf38995583d",
    "timestamp": "2022-05-24T07:03:36.758+00:00",
    "webhookId": 2477581984393092,
    "scope": "sheet",
    "scopeObjectId": 944130760173444,
    "events": [
        {
            "objectType": "cell",
            "eventType": "created",
            "rowId": 4256072584062852,
            "columnId": 5586258751711108,
            "userId": 7738337504061316,
            "timestamp": "2022-05-24T07:03:30.000+00:00"
        }
    ]
    }
 */
router.post('/', bodyParser, asyncHandler(async(req, res) => {
    const webhook_id = req.body.webhookId;
    const companyData = read_json.getCompanyColumns(webhook_id);
    const sheetId = companyData.companySheetId;
    const dataColumnId = companyData.companyDataColumnId;
    const listColumnId = companyData.companyListColumnId;
    const output = await updateRows(sheetId, dataColumnId, listColumnId);
    res.send(output);
    res.sendStatus(200);
}));

router.post('/:id', bodyParser, async(req, res) => {
    const companySheetId = req.params.id;
    const companyData = read_json.getCompanyColumns(companySheetId);
    console.log(companyData);
    if(companyData !== undefined) {
        const dataColumnId = companyData.companyDataColumnId;
        const listColumnId = companyData.companyListColumnId;
        const output = await updateRows(companySheetId, dataColumnId, listColumnId);
        res.send(output);
        res.sendStatus(200);
    } else {
        logging.err("companyData not valid. companyData : " + companyData);
        res.sendStatus(500);
    }

});
router.use((error, req, res, next) => {
    if (error) {
        logging.err(error);
        res.send(error);
        res.sendStatus(500);
    } else {
        res.sendStatus(200);
    }
});

module.exports = router;
