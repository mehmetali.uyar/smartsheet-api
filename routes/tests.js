const express = require('express'),
  router = express.Router(),
  smartsheet = require('../modules/smartsheet_url'),
  bodyParser = require('body-parser').json();
const constants = require("../constants/constants");
const sheet = require("../constants/sheet");
const logging = require("../modules/logging");

router.get('/', async (req, res, next) => {
  const wh = await smartsheet.getWebhooks();
  const sheets = await smartsheet.getSheets();
  const cols = await smartsheet.getSheet(sheet.id);
  res.send(cols);
});


router.use((error, req, res, next) => {
  if (error) {
    logging.err(error);
    res.sendStatus(500);
  } else {
    res.sendStatus(200);
  }
});
module.exports = router;
