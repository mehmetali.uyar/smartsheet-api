
const express = require('express'),
    sheets = require('./routes/sheets'),
    webhooks = require('./routes/webhooks'),
    columns = require('./routes/columns'),
    constants = require('./constants/constants'),
    tests = require('./routes/tests'),
    master_sheet = require('./routes/master_sheet');
    app = express();
    port = process.env.PORT || 3000;

if (constants.local) {
    app.use('/webhooks', webhooks);
    app.use('/sheets', sheets);
    app.use('/tests', tests);
}

app.use('/columns', columns);
app.use('/master_sheet', master_sheet);

app.listen(port, () => { });

module.exports = app;
