const fs = require("fs");
const logging = require("./logging");
const fileName = "company_columns.json";
const getCompanyColumns =  function (sheetId) {
    const data =  fs.readFileSync(fileName, 'utf8');
    let obj;
    if(data === undefined || data === "") {
        return undefined;
    }
    obj = JSON.parse(data);
    if(obj === undefined) {
        return undefined;
    }
    if (sheetId in obj) {
        return obj[sheetId];
    }
    return undefined;
}
const writeCompanyColumns = function (sheetId, dataColumnId, valueColumnId, webhookId){
    fs.readFile(fileName, 'utf8', function readFileCallback(err, data){
        if (err){
            logging.err(err);
        } else {
            let obj = JSON.parse(data); //now it an object
            obj[sheetId] = {
                companyDataColumnId: dataColumnId,
                companyListColumnId: valueColumnId,
                webhookId: webhookId
            }; //add some data
            const json = JSON.stringify(obj); //convert it back to json
            console.log(json);
            fs.writeFile(fileName, json, 'utf8', function writeFileCallback(err,data){
                if(err){
                    logging.err(err);
                }
            }); // write it back
        }});
}

module.exports = {getCompanyColumns, writeCompanyColumns}
