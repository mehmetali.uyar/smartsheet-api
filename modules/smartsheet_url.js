var constants = require('../constants/constants');
var axios = require('axios');
var logging = require('../modules/logging');
const smartsheet = () => {
    const main_url = "https://api.smartsheet.com/2.0/";
    const sheet_url = main_url + "sheets/";
    const webhook_url = main_url +  "webhooks/"
    const config = {
        headers: {
            Authorization: `Bearer ${constants.smartsheetToken}`,
            Accept: "application/json"
        }
    };

    return {
        getSheets: async () => {
            return await axios.get(sheet_url, config).then(response => {
                return response.data;
            });
        },

        getWebhooks: async () => {
            return await axios.get(webhook_url, config).then(response => {
                return response.data;
            });
        },
        getWebhook: async (id) => {
            return await axios.get(webhook_url + id, config).then(response => {
                return response.data;
            });
        },
        createWebhook: async (data) => {
            return await axios.post(webhook_url, data, config).then(response => {
                if(response.status === 200)
                {
                    return response.data;
                }
                else
                {
                    return response.status + "\n" + response;
                }
            })
        },
        updateWebhook: async (webhookId, body) => {
            let config = {
                method: 'put',
                url: webhook_url + webhookId,
                headers: {
                    Authorization: `Bearer ${constants.smartsheetToken}`,
                },
                data : body
            };
            return await axios(config).then(response => {
                return response.data;
            })
        },
        getRow: async (sheetId, rowId) => {
            return await axios.get(sheet_url + sheetId + "/rows/" + rowId, config).then(response => {
                return response.data;
            });
        },
        getSheet: async (id, columnId) => {
            var url = sheet_url + id;
            if(columnId)
            {
                url = url + "?columnIds=" + columnId;
            }
            return await axios.get(url, config).then(response => {
                return response.data;
            });
        },
        getColumns: async (sheetId, columnId) => {
            return await axios.get(sheet_url + sheetId + "/columns/" + columnId, config).then(response => {
                return response.data;
            });
        },
        updateColumn: async (sheetId, columnId, body) => {
            const data = {
                type:"PICKLIST",
                options: body
            };
            const config = {
                method: 'put',
                url: sheet_url + sheetId + "/columns/" + columnId,
                headers: {
                    Authorization: `Bearer ${constants.smartsheetToken}`,
                },
                data : data
            };
            return await axios(config).then(response => {
                return response.data;
            })
        }
    }
};

module.exports = smartsheet();
