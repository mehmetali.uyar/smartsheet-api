const logging = require('log-to-file');

const Logging = async (msg, isError, isJson) => {
    if (isError)
    {
        logging("! Error ! => " +  msg,'log.txt');
    }
    else if (isJson) {
        logging(JSON.stringify(msg), "json.json");
    }
    else {

        logging(msg,'log.txt');
    }
}

const log = (msg) =>  Logging(msg);

const err = (msg) =>  Logging(msg, true);

const json = (msg) => Logging(msg, false, true);


module.exports = { log, err, json };
